# Onternoot

Onternoot, a Layer 3 network protocol using Strings for host addresses. This is a proof of concept build to demonstrate why letters are not used as host addresses in place of numbers - but, that it is possible.

# Requirements

```
Lib: jpcap.jar v 0.6
    (a much older build than the current one in Maven/Sourceforge.
```