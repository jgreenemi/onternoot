package onternoot

import java.net.NetworkInterface
import jpcap._
import jpcap.packet._

object Onternoot {
  def main(args: Array[String]): Unit = {
    // First we define some values.

    val local_mac_address_string = "80:56:F2:4F:CE:E4"

    val local_mac_parts = local_mac_address_string.split(":")
    val local_mac_address_bytes: Array[Byte] = Array.ofDim[Byte](6)
    for(i <- local_mac_parts.indices) {
      local_mac_address_bytes(i) = Integer.parseInt(local_mac_parts(i), 16).toByte
    }

    for(i <- local_mac_address_bytes) {
      println(
        s"""Rebuilt:
           |${Byte.byte2int(i)} ->
           |${local_mac_address_bytes.map("%02X" format _).mkString}""".stripMargin)
    }

    println(
      s"""Results:\n
         |localmacparts ${local_mac_parts.mkString(",")} \n
         |localbytes ${local_mac_address_bytes.mkString(",")
      }""".stripMargin)


  }
}
